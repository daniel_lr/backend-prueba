const jwt = require("jsonwebtoken");
const secret = require("../config/config").SEED;

const mailSender = require("../middlewares/mail.utils");

const UserDataBank = require('../models/userdatabank.model');

function saveData(req, res) {

    const body = req.body; 

    if (body.iban != null && req.user != null) {

        const data = new UserDataBank({ 
            iban: body.iban,
            billingAddress: body.billingAddress,
            nif: body.nif,
            user: req.user._id
        });

        const temporarytoken = jwt.sign(
            { databank: data }, secret, { expiresIn: 14400 } 
        );

        const email = {
            from: "Localhost Staff, staff@localhost.com",
            to: req.user.email,
            subject: "Databank activation Link",
            text: "Databank",
            html: '<h1>Hello <strong> '+ req.user.name +'</strong></h1>'+
                   '<p>To save your bank details</p>' +
                   '<p>Please click on the following link:</p>' +
                   '<form action="http://localhost:3001/databank/validate?token=' + temporarytoken + '" method="POST">' +
                   '<input type="submit" value="Validate" />' +
                  '</form>'
        };

        mailSender.send( res, email );

    } else {
        res.status(201).json({
            ok: false,
            message: "Please, IBAN and User are required"
        });
    }
}

function validateUser(req, res) {
    const body = req.databank;

    const dataBank = new UserDataBank({ 
        iban: body.iban,
        billingAddress: body.billingAddress,
        nif: body.nif,
        user: body.user,
        active: true
    });

    dataBank.save( ( err, dataBankSaved ) => {

        if ( err ) { 
            return res.status(400).json({ 
                ok: false,
                mensaje: 'Error creating DataBank',
                errors: err
            });
        }

        res.redirect('http://localhost:3000/stored');

    });

}

module.exports = {
    saveData, validateUser
};