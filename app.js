const express = require('express');
const bodyParser = require('body-parser');

const { mongoose } = require('./resources/database');

const appRoutes = require('./routes/app.routes');
const userRoutes = require('./routes/user.routes');
const userDataBankRoutes = require('./routes/userdatabank.routes');

const app = express();

// middleware | CORS
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
    res.header('Access-Control-Allow-Methods', 'GET, POST, CREATE, OPTIONS, PUT, DELETE');
    res.header('Allow', 'GET, POST, CREATE, OPTIONS, PUT, DELETE');
    next();
});

const port = 3001;

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));
// parse application/json
app.use(bodyParser.json());

app.set('port', process.env.PORT || port);

// Routes (middlewares)
app.use( '/user', userRoutes );
app.use( '/databank', userDataBankRoutes );

app.use('/', appRoutes);

// To listen to the requests
app.listen(app.get('port'), () => { 
    console.log('Express server port: ' + port + ' \x1b[32m%s\x1b[0m','online');
    console.log('http://localhost:' + port);
});