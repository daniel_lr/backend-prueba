const mongoose = require('mongoose');

const URI = 'mongodb://localhost:27017/technicalTest';

mongoose.connect(URI, { useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: false });

let db = mongoose.connection;
db.on('error', ( err ) => { 
    console.error(err); 
    throw err; } 
);
db.once('open', () => { console.log('Database: \x1b[32m%s\x1b[0m','connected'); }); 

module.exports = mongoose;