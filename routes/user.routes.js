const express = require('express');

const middlewareAuth = require('../middlewares/auth');
const UserController = require('../controllers/user.controller');

const api = express();

api.post( '/', UserController.saveUser );
api.post( '/activate', middlewareAuth.checkToken, UserController.activateUser );
api.post( '/login', UserController.login );


module.exports = api;
