const bcrypt = require('bcryptjs');

const jwt = require('jsonwebtoken');
const secret = require('../config/config').SEED;

const mailSender = require('../middlewares/mail.utils');

const User = require('../models/user.model');

function saveUser(req, res) {
    const body = req.body;

  if (body.email != null && body.password != null) {

    const user = new User({
        name: body.name,
        email: body.email,
        password: bcrypt.hashSync(body.password, 10),
        phone: body.phone
    });

    const temporarytoken = jwt.sign(
        { user: user }, secret, { expiresIn: 14400 } 
    );

    const email = {
      from: 'Localhost Staff, staff@localhost.com',
      to: body.email,
      subject: 'Databank activation Link',
      text: 'Databank',
      html: '<h1>Hello <strong> '+ body.name +'</strong></h1>'+
             '<p>Thanks for registering</p>' +
             '<p>Please click on the link below to complete your activation:</p>' +
             '<form action="http://localhost:3001/user/activate?token=' + temporarytoken + '" method="POST">' +
             '<input type="submit" value="Activate account" />' +
            '</form>'
    };

    mailSender.send( res, email );

  } else {
    res.status(201).json({
      ok: false,
      message: 'Please, Email and Password are required'
    }); 
  }
}

function login(req, res) {

  const body = req.body;

    User.findOne({ email: body.email }, ( err, userDB ) => { 
        if ( err ) {
            return res.status(500).json({ 
                ok: false,
                message: 'User search failed',
            });
        }

        if ( !userDB ) {
            return res.status(400).json({
                ok: false,
                message: 'Bad credentials', 
                errors: err
            });
        }

        if ( !bcrypt.compareSync( body.password, userDB.password ) ) { 
            return res.status(400).json({ 
                ok: false,
                message: 'Bad credentials',
                errors: err
            });
        }

        userDB.password = ';-)';
        let token = jwt.sign({ user: userDB }, secret, { expiresIn: 14400 }); // 4h

        res.status(200).json({
            ok: true,
            user: userDB,
            token: token,
            id: userDB._id
        }); 
    });

}

function activateUser(req, res) {
    
    const user = new User({
        name: req.user.name,
        email: req.user.email,
        password: req.user.password,
        phone: req.user.phone,
        active: true
    });

    user.save((err, userSaved) => {
        if (err) {
          return res.status(400).json({
            ok: false,
            message: 'User already exists',
            errors: err
          });
        }

        res.redirect('http://localhost:3000/registered');

  });

}

module.exports = {
    saveUser, login, activateUser
};