const jwt = require('jsonwebtoken');
const SEED = require('../config/config').SEED;

// =============================================
//             Verify Token
// =============================================
exports.checkToken = function( req, res, next ) {

    const token = req.query.token; 

    jwt.verify( token, SEED, ( err, decoded ) => {

        if ( err ) { 
           
            return res.status(401).json({  
                ok: false,
                mensaje: 'Invalid token',
                errors: err
            });
        }
        req.user = decoded.user;
        req.databank = decoded.databank;
        next(); 

    });
};
