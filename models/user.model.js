const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');

mongoose.set('useCreateIndex', true); 

const Schema = mongoose.Schema;

const userSchema = new Schema({

    name: { type: String, required: [true, 'Name is required'] },
    email: { type: String, unique: true, required: [true, 'Email is required'] },
    password: { type: String, required: [true, 'Password is required'] },
    phone: { type: String, required: false },
    active: {type: Boolean, required: true, default: false }

});

userSchema.plugin( uniqueValidator, { message: '{PATH} must be unique'} );

module.exports = mongoose.model( 'Users', userSchema );