const mongoose = require('mongoose');

mongoose.set('useCreateIndex', true); 

const Schema = mongoose.Schema;

const userDataBankSchema = new Schema({

    iban: { type: String, required: [true, 'IBAN is required'] },
    billingAddress: { type: String, required: false },
    nif: { type: String, required: false },
    user: { type: Schema.Types.ObjectId, ref: 'Users'},
    active: { type: Boolean, required: true, default: false }

});

module.exports = mongoose.model( 'UserDataBank', userDataBankSchema );