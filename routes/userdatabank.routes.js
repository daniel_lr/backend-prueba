const express = require('express');

const middlewareAuth = require("../middlewares/auth");

const api = express();

const DataController = require('../controllers/userDataBank.controller');

api.post('/', middlewareAuth.checkToken, DataController.saveData);
api.post('/validate', middlewareAuth.checkToken, DataController.validateUser);

module.exports = api; 
